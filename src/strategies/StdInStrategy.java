package strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by amen on 8/17/17.
 */
public class StdInStrategy implements IInputStrategy {

    private Scanner scanner = new Scanner(System.in);

    public StdInStrategy() {
    }

    @Override
    public int getInt() {
        System.out.println("Wpisz integer:");
        return scanner.nextInt();
    }

    @Override
    public String getString() {
        System.out.println("Wpisz string:");
        return scanner.nextLine();
    }

    @Override
    public double getDouble() {
        System.out.println("Wpisz double:");
        return scanner.nextDouble();
    }

    @Override
    public void close() {
        scanner.close();
    }
}
